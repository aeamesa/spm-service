package com.tangocode.app;

import com.tangocode.rest.AuthenticationFilter;
import com.tangocode.rest.WebScraperRestService;

import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by amesa on 11/16/18.
 */

public class WebScraperServiceApplication extends Application {
    private Set<Object> singletons = new HashSet<Object>();

    public WebScraperServiceApplication() {
        // Register services
        singletons.add(new WebScraperRestService());
        singletons.add(new AuthenticationFilter());
    }

    @Override
    public Set<Object> getSingletons() {
        return singletons;
    }
}

