package com.tangocode.app;

import com.tangocode.jobscheduler.process.main.ScheduleStandAlone;

/**
 * Created by amesa on 11/27/18.
 */
public class Executor extends Thread{
    private String[] args ;

    public void setArgs(String[] args) {
        this.args = args;
    }

    public void run(){
        (new ScheduleStandAlone()).main(this.args);
    }


}


