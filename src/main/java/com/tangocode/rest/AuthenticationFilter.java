package com.tangocode.rest;

import com.tangocode.app.Secure;
import org.apache.http.HttpHeaders;

import javax.annotation.Priority;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;
import java.io.IOException;

/**
 * Created by amesa on 11/19/18.
 */
@Secure
@Provider
@Priority(Priorities.AUTHENTICATION)
public class AuthenticationFilter implements ContainerRequestFilter {

    private static final String REALM = "example";
    private static final String AUTHENTICATION_SCHEME = "token";


    public void filter(ContainerRequestContext requestContext) throws IOException {
        try {
            System.out.println("running filter function");
            // Get the Authorization header from the request
//            String authorizationHeader =
//                    requestContext.getHeaderString(HttpHeaders.AUTHORIZATION);
//
//            System.out.println("authorizationHeader " + authorizationHeader);
//
//            // Validate the Authorization header
//            if (!isTokenBasedAuthentication(authorizationHeader)) {
//                abortWithUnauthorized(requestContext);
//                return;
//            }
//
//            // Extract the token from the Authorization header
//            String token = authorizationHeader
//                    .substring(AUTHENTICATION_SCHEME.length()).trim();
//
//            System.out.println("el token " + token);
//
//
//            // Validate the token
//            validateToken(token);

        } catch (Exception e) {
            abortWithUnauthorized(requestContext);
        }
    }

    private boolean isTokenBasedAuthentication(String authorizationHeader) {

        // Check if the Authorization header is valid
        // It must not be null and must be prefixed with "token" plus a :
        // The authentication scheme comparison must be case-insensitive
        System.out.println("isTokenBasedAuthentication " + authorizationHeader +" " + AUTHENTICATION_SCHEME.toLowerCase());
        return authorizationHeader != null && authorizationHeader.toLowerCase()
                .startsWith("{\""+AUTHENTICATION_SCHEME.toLowerCase() + "\":");
    }

    private void abortWithUnauthorized(ContainerRequestContext requestContext) {
        System.out.println("abortWithUnauthorized " );
        // Abort the filter chain with a 401 status code response
        // The WWW-Authenticate header is sent along with the response
        requestContext.abortWith(
                Response.status(Response.Status.UNAUTHORIZED)
                        .header(HttpHeaders.WWW_AUTHENTICATE,
                                AUTHENTICATION_SCHEME + " realm=\"" + REALM + "\"")
                        .build());
    }

    private void validateToken(String token) throws Exception {
        System.out.println( "valdidate tiken!!!! " + token);
        // Check if the token was issued by the server and if it's not expired
        // Throw an Exception if the token is invalid
        //throw new Exception("invalid token!!! " + token);
    }
}