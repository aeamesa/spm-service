package com.tangocode.rest;
import com.tangocode.app.Executor;
import com.tangocode.app.Secure;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;
/**
 * Created by amesa on 11/16/18.
 */


@Path("/")
public class WebScraperRestService {
    /*
            option values:
            ALL:
            MISSED:
            DEALER:
            ERRORS:
     */

    @Secure
    @GET
    @Path("/webscraperReRun/{option}")
    public Response webscraperReRun(@PathParam("option") String option) {
        try {
            rerun(option,"");
        }catch (Exception e){
            e.printStackTrace();
            return Response.status(500).entity(e.getStackTrace().toString()).build();
        }
        return Response.status(200).entity("OK").build();
    }

    @Secure
    @GET
    @Path("/webscraperReRunDealer/{option}/{id}")
    public Response webscraperReRunDealer(@PathParam("option") String option, @PathParam("id") String id) {
        try {
           rerun(option,id);
        }catch (Exception e){
            e.printStackTrace();
            return Response.status(500).entity(e.getStackTrace().toString()).build();
        }
        return Response.status(200).entity("OK").build();
    }

    private void rerun(String option, String id){

            String[] args = new String[4];
            args[0] = System.getenv("TC_WS_JOB_DEF_WEBSCRAPER");
            args[1] = System.getenv("TC_WS_JOB_PRIORITY_WEBSCRAPER");
            args[2] = option;
            if(option.equals("DEALER")){
                args[3] = id;

            }
        Executor executor = new Executor();
            executor.setArgs(args);
            executor.start();
    }

}
